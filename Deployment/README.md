# BNP LOCAL DEPLOYMENT STEPS

## 1. Login to docker hub
docker login -u psdeveloper -p FBvAgfFHV8k8eHUh  //password changes frequently. Need to ask for valid password to be able to login.

## 2. Run Docker container with name “bnpdb” for this case.
   ```
   docker run -d -it  -p 1521:1521 --name bnpdb oracle12c:latest
   ```

## 3. Run SQL Developer and dd new connection with following configuration
```
   • Connection Name = “Any name”
   • Username = system
   • Password = oracle
   • Role = SYSDBA
   • Hostname = localhost
   • Port = 1521
   • SID = EE
```
and click Test. Once test is successful. Click on Connect.


## 4. Once connected to the database create table space by running following script by clicking on Run Script or  pressing f5 while selecting the scripts to run. Run following scripts.

```
CREATE TABLESPACE SEC_MASS_IDX01
DATAFILE 'SEC_MASS_IDX01_tab01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
CREATE TABLESPACE SEC_MASS_TAB01
DATAFILE 'SEC_MASS_TAB01_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
CREATE TABLESPACE ECC_HIST_LOB01
DATAFILE 'ECC_HIST_LOB01_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
CREATE TABLESPACE ECC_HIST_LOB02
DATAFILE 'ECC_HIST_LOB02_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
CREATE TABLESPACE ECC_HIST_LOB03
DATAFILE 'ECC_HIST_LOB03_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
CREATE TABLESPACE ECC_HIST_LOB04
DATAFILE 'ECC_HIST_LOB04_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
CREATE TABLESPACE ECC_LOB01
DATAFILE 'ECC_LOB01_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
CREATE TABLESPACE ECC_LOB02
DATAFILE 'ECC_LOB02_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
PROMPT Creating Tablespace 'ECC_TAB01'
CREATE  TABLESPACE ECC_TAB01
DATAFILE 'HECC_TAB01_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
PROMPT Creating Tablespace 'ECC_TAB02'
CREATE TABLESPACE ECC_TAB02
DATAFILE 'ECC_TAB02_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
PROMPT Creating Tablespace 'ECC_HIST_TAB01'
CREATE TABLESPACE ECC_HIST_TAB01
DATAFILE 'ECC_HIST_TAB01_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
PROMPT Creating Tablespace 'ECC_IDX01'
CREATE TABLESPACE ECC_IDX01
DATAFILE 'ECC_IDX01_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
PROMPT Creating Tablespace 'ECC_HIST_IDX01'
CREATE TABLESPACE ECC_HIST_IDX01
DATAFILE 'ECC_HIST_IDX01_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
PROMPT Creating Tablespace 'ECC_HIST_LOB01'
CREATE TABLESPACE ECC_HIST_LOB01
DATAFILE 'ECC_HIST_LOB01_01' SIZE 100M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 32M
BLOCKSIZE 16K
/
PROMPT Creating Tablespace 'ECC_HIST_LOB02'
CREATE TABLESPACE ECC_HIST_LOB02
DATAFILE 'ECC_HIST_LOB02_01' SIZE 100M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 32M
BLOCKSIZE 16K
/
PROMPT Creating Tablespace 'ECC_HIST_LOB03'
CREATE TABLESPACE ECC_HIST_LOB03
DATAFILE 'ECC_HIST_LOB03_01' SIZE 100M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 32M
BLOCKSIZE 16K
/
PROMPT Creating Tablespace 'ECC_HIST_LOB04'
CREATE TABLESPACE ECC_HIST_LOB04
DATAFILE 'ECC_HIST_LOB04_01' SIZE 100M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 32M
BLOCKSIZE 16K
/
PROMPT Creating Tablespace 'ECC_LOB01'
CREATE TABLESPACE ECC_LOB01
DATAFILE 'ECC_LOB01_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 4M
BLOCKSIZE 16K
/
PROMPT Creating Tablespace 'ECC_LOB02'
CREATE TABLESPACE ECC_LOB02
DATAFILE 'ECC_LOB02_01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 4M
BLOCKSIZE 16K
/
PROMPT Creating Tablespace 'ECC_INT_IDX01'
CREATE TABLESPACE ECC_INT_IDX01
DATAFILE 'ECC_INT_IDX01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
PROMPT Creating Tablespace 'ECC_INT_TAB01'
CREATE TABLESPACE ECC_INT_TAB01
DATAFILE 'ECC_INT_TAB01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
PROMPT Creating Tablespace 'ECC_LOG_IDX01'
CREATE TABLESPACE ECC_LOG_IDX01
DATAFILE 'ECC_LOG_IDX01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
NOLOGGING
/
PROMPT Creating Tablespace 'ECC_LOG_TAB01'
CREATE TABLESPACE ECC_LOG_TAB01
DATAFILE 'ECC_LOG_TAB01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
PROMPT Creating Tablespace 'SEC_TAB01'
CREATE TABLESPACE SEC_TAB01
DATAFILE 'SEC_TAB01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
PROMPT Creating Tablespace 'SEC_IDX01'
CREATE TABLESPACE SEC_IDX01
DATAFILE 'SEC_IDX01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/
PROMPT Creating Tablespace 'SEC_LOG_TAB01'
CREATE TABLESPACE SEC_LOG_TAB01
DATAFILE 'SEC_LOG_TAB01' SIZE 50M AUTOEXTEND ON NEXT 10M
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
SEGMENT SPACE MANAGEMENT AUTO
/

5. Create data_dump/dbssec directory in container and move database files & script from host to docker container
   sudo docker cp dbssec bnpdb:/u01/data_dump/dbssec/
   sudo docker cp DATABASE bnpdb:/u01/data_dump/dbssec/



6. Create users BNP_ECC, BNP_SEC, and BNP_SHDW

CREATE USER BNP_ECC IDENTIFIED BY BNP_ECC DEFAULT TABLESPACE ECC_TAB01 TEMPORARY TABLESPACE TEMP;
CREATE USER BNP_SEC IDENTIFIED BY BNP_SEC DEFAULT TABLESPACE ECC_TAB01 TEMPORARY TABLESPACE TEMP;
CREATE USER BNP_SHDW IDENTIFIED BY BNP_SHDW DEFAULT TABLESPACE ECC_TAB01 TEMPORARY TABLESPACE TEMP;

7.  Grant privileges to all users(BNP_SHDW, BNP_SEC, BNP_ECC) by running following script in SQL Developer. Replace all BNP_SHDW using find and replace in SQL Developer with other User schema names respectively until all of them are granted the privilege.
    GRANT CREATE SESSION TO BNP_SHDW
    /
    GRANT QUERY REWRITE TO BNP_SHDW
    /
    GRANT UNLIMITED TABLESPACE TO BNP_SHDW
    /
    GRANT RESOURCE TO BNP_SHDW
    /
    GRANT CONNECT TO BNP_SHDW
    /
    GRANT SELECT_CATALOG_ROLE TO BNP_SHDW
    /
    GRANT SELECT ANY DICTIONARY TO BNP_SHDW
    /
    GRANT CREATE ANY SNAPSHOT TO BNP_SHDW
    /
    GRANT CREATE ANY SYNONYM TO BNP_SHDW
    /
    GRANT CREATE ANY view TO BNP_SHDW
    /
    GRANT CREATE database link TO BNP_SHDW
    /
    GRANT CREATE ANY CONTEXT TO BNP_SHDW
    /
    grant execute on DBMS_CRYPTO to BNP_SHDW
    /
    GRANT CREATE SESSION TO BNP_SHDW
    /
    GRANT QUERY REWRITE TO BNP_SHDW
    /
    GRANT UNLIMITED TABLESPACE TO BNP_SHDW
    /
    GRANT RESOURCE TO BNP_SHDW
    /
    GRANT CONNECT TO BNP_SHDW
    /
    GRANT SELECT_CATALOG_ROLE TO BNP_SHDW
    /
    GRANT SELECT ANY DICTIONARY TO BNP_SHDW
    /
    GRANT CREATE ANY SNAPSHOT TO BNP_SHDW
    /
    GRANT CREATE ANY SYNONYM TO BNP_SHDW
    /
    GRANT CREATE ANY view TO BNP_SHDW
    /
    GRANT CREATE database link TO BNP_SHDW
    /
    GRANT CREATE ANY CONTEXT TO BNP_SHDW
    /
    grant execute on DBMS_CRYPTO to BNP_SHDW
    /
```
## 7. Run runscript.sh containing below instructions inside container:

### SEC RUN
```
cd /u01/data_dump/dbssec/
sh ./run_all.sh BNP_SEC BNP_SEC localhost:1521/EE.oracle.docker . sqlplus . oracle true false '/u01/app/oracle/oradata/' BNP_SHDW BNP_SHDW BNP_ECC 7be38a06
```
#ECC RUN
cd /u01/data_dump/DATABASE/ECC_CORE
sh ./run_all.sh BNP_ECC BNP_ECC BNP_SEC BNP_SEC localhost:1521/EE.oracle.docker /u01/data_dump/DATABASE/ECC_CORE/ sqlplus /tmp 18 false oracle true 10 false '/u01/app/oracle/oradata' BNP_ECC BNP_ECC false BNP_SHDW BNP_SHDW false false 7be38a06
sh ./run_app_scripts.sh BNP_ECC BNP_ECC localhost:1521/EE.oracle.docker 10 18 true false BNP_SEC BNP_SEC
sh ./run_app_scripts_shdw.sh BNP_SHDW BNP_SHDW localhost:1521/EE.oracle.docker 10 18 true

8. check for invalid objects by running following command in SQL Developer
   select OWNER,count(*) from dba_objects where status='INVALID' group by OWNER;

9. compile invalid objects by running following command in SQL Developer. Replace Schema_Name with appropriate username create above
   EXEC DBMS_UTILITY.compile_schema(schema => 'Schema_Name');

10. Update schema name, password and database url:
    at ECC/META-NF/context.html

<Resource auth="Container" driverClassName="oracle.jdbc.OracleDriver"
maxActive="20" maxIdle="10" maxWait="-1" name="jdbc/ECC" username="BNP_ECC"
password="BNP_ECC" type="javax.sql.DataSource"
url="jdbc:oracle:thin:@172.17.0.2:1521/EE.oracle.docker"
validationQuery="select 1 from dual" />

<Resource auth="Container" driverClassName="oracle.jdbc.OracleDriver"
maxActive="20" maxIdle="10" maxWait="-1" name="jdbc/PSSEC" username="BNP_SEC"
password="BNP_SEC" type="javax.sql.DataSource"
url="jdbc:oracle:thin:@172.17.0.2:1521/EE.oracle.docker"
validationQuery="select 1 from dual" />

<Resource auth="Container" driverClassName="oracle.jdbc.OracleDriver"
maxActive="20" maxIdle="10" maxWait="-1" name="jdbc/PSSHDW" username="BNP_SHDW"
password="BNP_SHDW" type="javax.sql.DataSource"
url="jdbc:oracle:thin:@172.17.0.2:1521/EE.oracle.docker"
validationQuery="select 1 from dual" />

at PSSEC/META-INF/context.xml

<Resource auth="Container" driverClassName="oracle.jdbc.OracleDriver"
maxActive="20" maxIdle="10" maxWait="-1" name="jdbc/PSSHDW" username="BNP_SEC"
password="BNP_SEC" type="javax.sql.DataSource"
url="jdbc:oracle:thin:@172.17.0.2:1521/EE.oracle.docker"
validationQuery="select 1 from dual" />

at PSSHDW/META-INF/context.xml

<Resource auth="Container" driverClassName="oracle.jdbc.OracleDriver"
maxActive="20" maxIdle="10" maxWait="-1" name="jdbc/PSSHDW" username="BNP_SHDW"
password="BNP_SHDW" type="javax.sql.DataSource"
url="jdbc:oracle:thin:@172.17.0.2:1521/EE.oracle.docker"
validationQuery="select 1 from dual" />

11. Update APPINST_INST_URL table for BNP_SEC schema and BNP_SHDW schema with appropriate url. In our case
    http://localhost:8080/PSSHDW/ for PS-Approve
    http://localhost:8080/PSSEC/ for PS-Security
    http://localhost:8080/ECC/ for PS-ECC

12. Set ${APP_ID} for PSSEC to 1, for PSSHDW to 2 a at following entry of PSSHDW/PSSEC/WEB-INF/web.xml
    PSSHDW/PSSHDW/WEB-INF/web.xml

<env-entry>
<env-entry-name>ent/ApplicationId</env-entry-name>
<env-entry-type>java.lang.String</env-entry-type>
<env-entry-value>2</env-entry-value>
</env-entry>


13. copy com.progressoft.spirit.bnp.security-ps-sec-api-v1.0.1.jar from PSSEC/WEB-INF/lib/ to ECC/WEB-INF/lib/ to fix Security pages not loading.



14. To configure Image Capture
    a. In ECC_PARAM
    update OcxCabName, OcxVersion, OcxClassId with appropriate information found in Image Capture .cab file→ ImageCapture_Vunified-eccax-20710.0.inf (name may differ as per version.) from the below field:

[ImageCapture_Vunified-eccax-20710.0.ocx]
file-win32-x86=thiscab
clsid={5f3d0c99-fc57-4f03-b1f1-adae73721e96}
FileVersion=20,7,10,0
RegisterServer=yes

If trigger show issues while committing, disable trigger temporarily, commit and enable trigger again.

