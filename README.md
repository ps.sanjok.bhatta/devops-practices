<a name="readme-top"></a>
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/progressoft/support/teamnepalrepo/pod-c/bnp">
    <img src="https://www.progressoft.com/assets/img/logo/logo.svg" alt="Logo" width="100" height="100">
  </a>

<h3 align="center">BNP ECC/ONUS | Support & Implementation</h3>

<p alight="center">United Arab Emirates</p>
  <p align="center">
    Welcome to BNP ECC Support & Implementation Project.
    <br />
    <a href="https://gitlab.com/progressoft/support/teamnepalrepo/pod-c/bnp/-/issues"><strong>Gitlab Issues»</strong></a>
    .
    <a href="https://support.progressoft.com/dash/projects/10429"><strong>Support Portal | AE00BNP ECC </strong></a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#application-server-details">ECC Production Application Server Details</a></li>
        <li><a href="#database-server-details">ECC Production Database Server Details</a></li>
        <li><a href="#communication-server-details">ECC Production Communication Server Details</a></li>
        <li><a href="#onus">ONUS Server Details</a></li>
      </ul>
    </li>
    <li><a href="/Deployment/README.md">ECC Local Deployment Steps</a></li>
    <li><a href="#ecc-releases">ECC Releases</a></li>
    <li><a href="#wars-and-files">WARS and Files</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
This document shall provide the operational information about BNP Production environment of PS-ECC, PS-Security, ONUS and  Communication services which is recently upgraded for the SSO implementation. 

<strong>Person of contact at Bank side<strong>
* Name: Mohammad Rafi
* Preferred Mode of Contact: Whatsapp and Support Ticket
* Whatsapp Number: +97338420727
* Email: mohammed.rafi@bnpparibas.com


### ECC Production Application Server Details

* Apache tomcat 9.0.34 with JDK 1.8 is running on the server distributed as follows: 
* Tomcat Path: D:\APP\ECC			
* JDK Path:	D:\APP\Java
* Hostname: DXB00100094.ae.net.intra
* Application profiles:  D:\APP\ECC\

### ECC Production Database Server Details
* Database Server IP: dxbecc.ae.net.intra
* Database Listener Port:  1521
* Service Name:  ECC
* Application Schemas: ICCS_SSOAPP18, ICCS_SSOSEC18, ICCS_SSOSHDW18

### ECC Producion Communication Server Details
The communication services are java executable which are responsible for sending and receiving cheques to and from central bank. 
* Hostname:  DXBS30000024.ae.net.intra
* Services location:  D:\FTP POP
* Services log location: D:\FTP POP
* Envelops Path: D:\Envelopes

<strong>Note: All FTP population communication services are configured in FireDaemon using below command inside respective .bat files to start those services.</strong>
```
java -jar -Xms256m -Xmx512m service-discovery-1.0.jar --logstash.host=logstash --logstash.port=5000 --spring.zipkin.baseUrl=http://zipkin-server:9411/ --host=localhost
java -jar -Xms256m -Xmx512m config-server-1.0.jar --sdHost1=localhost --sdPort1=9999 --logstash.host=logstash --logstash.port=5000 --spring.zipkin.baseUrl=http://zipkin-server:9411/ --configPath=./config-repo --host=localhost 
java -jar -Xms256m -Xmx512m x937-service-1.0.jar --sdHost1=localhost --sdPort1=9999 --logstash.port=5000 --spring.zipkin.baseUrl=http://zipkin-server:9411/ --host=localhost >>x937-service.log
java -jar -Xms256m -Xmx512m bfd-export-1.0.jar --sdHost1=localhost --sdPort1=9999 --logstash.port=5000 --spring.zipkin.baseUrl=http://zipkin-server:9411/ --configPath=./config-repo --host=localhost
java -jar -Xms256m -Xmx512m bfd-import-1.0.jar --sdHost1=localhost --sdPort1=9999 --logstash.port=5000 --spring.zipkin.baseUrl=http://zipkin-server:9411/ --configPath=./config-repo --host=localhost
java -jar -Xms256m -Xmx512m pay-export-1.0.jar --sdHost1=localhost --sdPort1=9999 --logstash.port=5000 --spring.* * zipkin.baseUrl=http://zipkin-server:9411/ --configPath=./config-repo --host=localhost
java -jar -Xms256m -Xmx512m pay-import-1.0.jar --sdHost1=localhost --sdPort1=9999 --logstash.port=5000 --spring.zipkin.baseUrl=http://zipkin-server:9411/ --configPath=./config-repo --host=localhost
java -jar -Xmx512m -Xms512m ftp-communication-1.0.jar --server.port=9993
```

The logs are generated in D:\FTP POP\Logs folder where all of the logs will rotate after it reaches the size of 50Mb and it will retain the logs of 3 days for Ftp-communication, bfd-import, bfd-export, pay-import, pay-export, service-discovery and config-server services.

### ONUS

ONUS is installed in same server as ECC (In both UAT and Production).

<strong>ONUS Production Application Server Details:</strong> 
* Apache tomcat 9.0.34 with JDK 1.8 is running on the server distributed as follows: 
* Tomcat Path: D:\APP\ECC			
* JDK Path:	D:\APP\Java
* Hostname: DXB00100094.ae.net.intra
* Application profiles:  D:\APP\ONUS

<strong>ONUS Database Server Details:</strong> 
* Database Server IP: dxbecc.ae.net.intra
* Database Listener Port:  1521
* Service Name:  ECC

<strong>The schemas are:</strong> 
* ICCS_SSOONUS18
* ICCS_SSOONUSSEC18
* ICCS_SSOONUSSHDW18

<!-- ECC LOCAL DEPLOYMENT STEPS -->
## ECC Local Deployment Steps

Please find the ECC local deployment steps document <a href="https://gitlab.com/ps.sanjok.bhatta/devops-practices/-/tree/main/Deployment">here</a>


<figure class="video_container">
<iframe src="https://gitlab.com/progressoft/support/teamnepalrepo/pod-c/bnp/activity" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## ECC Releases
* Current Version: 1.1.0 (2021-06-08)
* Check other releases here: https://gitlab.com/progressoft/checks/ecc-uae/bnp/bnp-ecc/-/releases

## WARS and Files
* WARS and Installation files can be found here: <a href="https://clususcom.sharepoint.com/:f:/s/Clusus_Nepapl/EuMZvUBJ2vJJlI1_S3ZtFWkBIcNbfAeT1R-PirN2uBvH_w?e=uV8Qji">BNP CLUSUS ONEDRIVE</a>
<p align="right">(<a href="#readme-top">back to top</a>)</p>



